import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token'),
    logoUrl: '',
    uploadUrl: process.env.VUE_APP_API_HOST + 'api/admin/common/uploadInfo',
    uploadUrlPic: process.env.VUE_APP_API_HOST + 'api/upload/pics'
  },
  mutations: {
      setLogoUrl(state,url){
          state.logoUrl = url;
      }
  },
    getters: {
        getLogoUrl: state => {
            return state.logoUrl;
        }
    },
  actions: {

  }
})
