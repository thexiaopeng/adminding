// vue.config.js
module.exports = {
  devServer: {
    port: 3000,
      proxy: {
          // 配置跨域
          "/api": {
              target: "http://localhost:8090/",
              // secure: false,
              pathRewrite: {
                  "^/api": ""
              },
              changeOrigin: true,
              logLevel: "debug"
          }
      }
    // proxy: 'http://c.adbpx.com/api/manager/'
  }
};