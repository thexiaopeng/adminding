import axios from 'axios'

export async function login(data) {
  const  result = await axios.post('api/admin/user/session', data);
  return result
}
