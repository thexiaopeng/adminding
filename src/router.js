import Vue from 'vue'
import Router from 'vue-router'
import store from "./store";

Vue.use(Router);

export default new Router({
    mode: 'hash',
    routes: [
        {
            path: '/',
            redirect: '/swipe/management'
        },
        {
            path: '/home',
            name: 'home',
            meta: {
                requireAuth: true,
            },
            component: () => import('./views/Index.vue')
        },
        {
            path: '/login',
            name: 'login',
            component: () => import('./views/Login.vue'),
            meta: {
                requireAuth: false,
            }
        },
        {
            path: '/user/management',
            name: 'userManagement',
            component: () => import('./views/userManagement/User.vue'),
            meta: {
                requireAuth: true,
            }
        },
        {
            path: '/swipe/management',
            name: 'swipeManagement',
            component: () => import('./views/swipeManagement/Swipe.vue'),
            meta: {
                requireAuth: true,
            }
        },
        {
            path: '/zixun/type',
            name: 'ziXunType',
            component: () => import('./views/typeManagement/ZiXun.vue'),
            meta: {
                requireAuth: true,
            }
        },
        {
            path: '/kecheng/type',
            name: 'keChengType',
            component: () => import('./views/typeManagement/KeCheng.vue'),
            meta: {
                requireAuth: true,
            }
        },
        {
            path: '/file/type',
            name: 'fileType',
            component: () => import('./views/typeManagement/File.vue'),
            meta: {
                requireAuth: true,
            }
        },
        {
            path: '/zixun/content',
            name: 'ziXunContent',
            component: () => import('./views/contentManagement/ZiXun.vue'),
            meta: {
                requireAuth: true,
            }
        },
        {
            path: '/problem/content',
            name: 'problemContent',
            component: () => import('./views/contentManagement/Problem.vue'),
            meta: {
                requireAuth: true,
            }
        },
        {
            path: '/curriculum/content',
            name: 'curriculumContent',
            component: () => import('./views/contentManagement/Curriculum.vue'),
            meta: {
                requireAuth: true,
            }
        },
        {
            path: '/file/content',
            name: 'fileContent',
            component: () => import('./views/contentManagement/File.vue'),
            meta: {
                requireAuth: true,
            }
        }
    ]
})
