import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import store from './store'
import router from './router'
import Router from 'vue-router'

// axios是promise实现的，提到promise，首先应该想到IE不支持，所以应该先加个垫片，给IE做下兼容性处理
import 'babel-polyfill'
import  VueQuillEditor from 'vue-quill-editor'
// require styles 引入样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor);
// 引入Axios
//引入axios
import axios from './axios'

//放大组建
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'
Vue.use(Viewer);

// 引入bootstrap
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/js/bootstrap'
import 'popper.js/dist/esm/popper'

// 引入font-awesome
import "font-awesome/css/font-awesome.css";

// 引入公共样式
import './common.css'

//  引入vue-amap
import VueAMap from 'vue-amap';

// 引入elementUI
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 全局路由守卫
router.beforeEach((to, from, next) => {
    const nextRoute = ['userManagement','swipeManagement','ziXunType','keChengType','fileType','ziXunContent','problemContent','curriculumContent','fileContent'];
    let isLogin = localStorage.getItem('token');  // 是否登录
    // 未登录状态；当路由到nextRoute指定页时，跳转至login
    if (nextRoute.indexOf(to.name) >= 0) {
        if (!isLogin) {
            router.push({ name: 'login' })
        }
    }
    // 已登录状态；当路由到login时，跳转至home
    if (to.name === 'login') {
        if (isLogin) {
            router.push({ name: 'swipeManagement' });
        }
    }
    next();
})
Vue.use(ElementUI);
Vue.use(VueAMap);
Vue.use(Vuex);
Vue.use(Router);
Vue.use(axios);
Vue.config.productionTip = false;



new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');


